## General info
This project is homework project for my recruitment process. Task is developing a solution to find classes by pattern (similar to the IntelliJ IDEA Ctrl + N shortcut)

## Usage
To run this project use :

```bash
~$ java -jar ClassFinder.jar <filename> <pattern>
```

