package net.baghirzade.classfinder;

import java.util.List;

public interface ClassFinder {

    List<String> find(String filePath, String pattern);

    List<String> find(List<String> classNames, String pattern);

}
