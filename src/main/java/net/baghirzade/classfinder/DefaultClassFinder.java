package net.baghirzade.classfinder;

import net.baghirzade.classfinder.name.ClassName;
import net.baghirzade.classfinder.pattern.Pattern;
import net.baghirzade.classfinder.util.FileUtil;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultClassFinder implements ClassFinder{

    @Override
    public List<String> find(String filePath, String givenPattern) {

        return this.find(FileUtil.readLines(filePath), givenPattern);
    }

    @Override
    public List<String> find(List<String> givenClassNames, String givenPattern) {

        Pattern pattern = new Pattern(givenPattern);

        return givenClassNames.stream()
                .map(ClassName::new)
                .filter(pattern::match)
                .sorted()
                .map(ClassName::getFullClassName)
                .collect(Collectors.toList());
    }
}
