package net.baghirzade.classfinder;

import net.baghirzade.classfinder.exception.IncorrectExecutionException;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        if (args.length < 2) throw new IncorrectExecutionException();

        ClassFinder classFinder = new DefaultClassFinder();
        List<String> mp = classFinder.find(args[0], args[1]);

        mp.forEach(System.out::println);
    }

}
