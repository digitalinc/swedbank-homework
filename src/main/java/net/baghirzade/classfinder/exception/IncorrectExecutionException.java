package net.baghirzade.classfinder.exception;

public class IncorrectExecutionException extends RuntimeException{

    private static final String DEFAULT_MESSAGE = "Execution commands are not sufficient@";

    public IncorrectExecutionException() {
        super(DEFAULT_MESSAGE);
    }

    public IncorrectExecutionException(String message) {
        super(message);
    }
}
