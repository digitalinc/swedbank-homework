package net.baghirzade.classfinder.exception;

public class UndefinedPatternException extends RuntimeException{

    private static final String DEFAULT_MESSAGE = "Pattern is not set or is incorrect";

    public UndefinedPatternException() {
        super(DEFAULT_MESSAGE);
    }

    public UndefinedPatternException(String message) {
        super(message);
    }
}
