package net.baghirzade.classfinder.exception;

public class UnknownClassFinderTypeException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "ClassFinderType is not set or is incorrect";

    public UnknownClassFinderTypeException() {
        super(DEFAULT_MESSAGE);
    }

    public UnknownClassFinderTypeException(String message) {
        super(message);
    }
}
