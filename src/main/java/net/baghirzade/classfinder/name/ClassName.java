package net.baghirzade.classfinder.name;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ClassName implements Comparable<ClassName>{
    private String className;
    private String fullClassName;
    private List<ClassNamePart> parts = new ArrayList<>();

    public ClassName() {}

    public ClassName(String name) {
        int lastIndexOfPoint = name.lastIndexOf('.');

        if (!isValid(name)) return;

        ClassNamePart temporaryPart = new ClassNamePart();
        StringBuilder temporaryStringBuilder = new StringBuilder();

        for (Character singleChar : name.substring(lastIndexOfPoint + 1).toCharArray()) {
            if(Character.isUpperCase(singleChar)) {
                if (temporaryPart.getFirstLetter() != null) {
                    temporaryPart.setLowerLetters(temporaryStringBuilder.toString());

                    parts.add(temporaryPart);
                }

                // reset temporaryPart
                temporaryPart = new ClassNamePart();
                temporaryStringBuilder.replace(0, temporaryStringBuilder.length(), "");

                temporaryPart.setFirstLetter(singleChar);
            } else {
                temporaryStringBuilder.append(singleChar);
            }
        }

        if (temporaryStringBuilder.length() > 0) {
            temporaryPart.setLowerLetters(temporaryStringBuilder.toString());
        }

        // add last part
        parts.add(temporaryPart);

        className = name.substring(lastIndexOfPoint + 1);
        fullClassName = name;
    }

    public ClassName(String className, String name, List<ClassNamePart> parts) {
        this.className = className;
        this.fullClassName = name;
        this.parts = parts;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFullClassName() {
        return fullClassName;
    }

    public void setFullClassName(String fullClassName) {
        this.fullClassName = fullClassName;
    }

    public List<ClassNamePart> getParts() {
        return parts;
    }

    public void setParts(List<ClassNamePart> parts) {
        this.parts = parts;
    }

    @Override
    public int compareTo(ClassName name) {
        return className.compareTo(name.getClassName());
    }

    @Override
    public String toString() {
        return fullClassName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassName name = (ClassName) o;
        return className.equals(name.className);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullClassName);
    }

    private boolean isValid(String name) {
        int lastIndexOfPoint = name.lastIndexOf('.');
        return lastIndexOfPoint != -1 && name.length() != lastIndexOfPoint + 1;
    }
}
