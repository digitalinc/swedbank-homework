package net.baghirzade.classfinder.name;

public class ClassNamePart {
    private Character firstLetter;
    private String lowerLetters;

    public ClassNamePart() {}

    public ClassNamePart(Character firstLetter, String lowerLetters) {
        this.firstLetter = firstLetter;
        this.lowerLetters = lowerLetters;
    }


    public Character getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(Character firstLetter) {
        this.firstLetter = firstLetter;
    }

    public String getLowerLetters() {
        return lowerLetters;
    }

    public void setLowerLetters(String lowerLetters) {
        this.lowerLetters = lowerLetters;
    }
}
