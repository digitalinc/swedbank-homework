package net.baghirzade.classfinder.pattern;

import net.baghirzade.classfinder.exception.UndefinedPatternException;
import net.baghirzade.classfinder.name.ClassName;
import net.baghirzade.classfinder.name.ClassNamePart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pattern {

    List<PatternPart> parts = new ArrayList<>();
    Boolean isLastCharSpace = false;

    public Pattern() {}

    public Pattern(String pattern) {
        pattern = correctPattern(pattern);

        if (pattern.length() < 1 || !Character.isUpperCase(pattern.charAt(0))) throw new UndefinedPatternException();

        PatternPart temporaryPart = new PatternPart();
        StringBuilder temporaryStringBuilder = new StringBuilder("");

        for (int i = 0; i < pattern.length(); i++) {

            if (Character.isUpperCase(pattern.charAt(i))) {

                if (temporaryStringBuilder.length() > 0
                        || temporaryPart.getFirstLetter() != null) {
                    temporaryPart.setLowerLetters(temporaryStringBuilder.toString());
                    parts.add(temporaryPart);

                    temporaryPart = new PatternPart();
                    temporaryStringBuilder.replace(0, temporaryStringBuilder.length(), "");
                }

                temporaryPart.setFirstLetter(pattern.charAt(i));

            } else if (pattern.charAt(i) == ' ') {
                if (i == pattern.length() - 1) {
                    isLastCharSpace = true;
                } else {
                    throw new UndefinedPatternException("Incorrect usage of space character");
                }
            } else {
                temporaryStringBuilder.append(pattern.charAt(i));
            }
        }

        temporaryPart.setLowerLetters(temporaryStringBuilder.toString());

        parts.add(temporaryPart);
    }

    public Pattern(List<PatternPart> parts) {
        this.parts = parts;
    }

    public List<PatternPart> getParts() {
        return parts;
    }

    public void setParts(List<PatternPart> parts) {
        this.parts = parts;
    }

    public static String correctPattern(String pattern) {
        boolean isLowerCase = true;

        for (char singleChar: pattern.toCharArray()) {
            if (Character.isUpperCase(singleChar)) {
                isLowerCase = false;
                break;
            }
        }

        return isLowerCase ? pattern.toUpperCase() : pattern;
    }

    public boolean match(ClassName className) {
        if (isLastCharSpace) {
            return matchInversely(className);
        } else {
            return matchStraight(className);
        }
    }

    public boolean matchStraight(ClassName className) {
        boolean isMatch = true;

        int classNamePartPointer = 0;
        for (PatternPart part : parts) {

            isMatch = false;
            while (className.getParts().size() > classNamePartPointer) {
                if (matchParts(part, className.getParts().get(classNamePartPointer))) {
                    classNamePartPointer++;
                    isMatch = true;
                    break;
                }

                classNamePartPointer++;
            }
        }

        return isMatch;
    }


    public boolean matchInversely(ClassName className) {
        Collections.reverse(parts);
        Collections.reverse(className.getParts());

        if (!matchParts(parts.get(0), className.getParts().get(0)))
            return false;


        boolean isMatch = true;

        int classNamePartPointer = 1;
        for (int partNo = 1; partNo < parts.size(); partNo++ ) {

            isMatch = false;
            while (className.getParts().size() > classNamePartPointer) {
                if (matchParts(parts.get(partNo), className.getParts().get(classNamePartPointer))) {
                    classNamePartPointer++;
                    isMatch = true;
                    break;
                }

                classNamePartPointer++;
            }
        }

        return isMatch;
    }

    private static boolean matchParts(PatternPart patternPart, ClassNamePart classNamePart) {
        if (patternPart.getFirstLetter() == classNamePart.getFirstLetter()){
            return matchLowerLetters(patternPart.getLowerLetters(), classNamePart.getLowerLetters());
        }else {
            return false;
        }
    }

    private static boolean matchLowerLetters(String patternPartLetters, String classNamePartLetters) {

        boolean matches = true;
        Character charAfterAsterisk = null;

        int classNamePartPointer = 0;
        for (int patternPartPointer = 0; patternPartPointer < patternPartLetters.length() ; patternPartPointer++) {
            if (patternPartLetters.charAt(patternPartPointer) == '*') {
                charAfterAsterisk = patternPartLetters.length() > patternPartPointer + 1
                        ? patternPartLetters.charAt(patternPartPointer + 1) : null;
                classNamePartPointer++;
            } else if (classNamePartLetters.length() > classNamePartPointer
                    && patternPartLetters.charAt(patternPartPointer)
                        == classNamePartLetters.charAt(classNamePartPointer)) {
                classNamePartPointer++;
            } else {
                if (charAfterAsterisk != null) {
                    return matchLowerLetters(patternPartLetters.substring(patternPartPointer + 1)
                            , classNamePartLetters.substring(classNamePartPointer));
                }else {
                    matches = false;
                    break;
                }
            }
        }

        return matches;
    }
}
