package net.baghirzade.classfinder.pattern;

public class PatternPart {

    private Character firstLetter;
    private String lowerLetters;

    public PatternPart() {
    }

    public PatternPart(Character firstLetter, String lowerLetters) {
        this.firstLetter = firstLetter;
        this.lowerLetters = lowerLetters;
    }

    public Character getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(Character firstLetter) {
        this.firstLetter = firstLetter;
    }

    public String getLowerLetters() {
        return lowerLetters;
    }

    public void setLowerLetters(String lowerLetters) {
        this.lowerLetters = lowerLetters;
    }

}
