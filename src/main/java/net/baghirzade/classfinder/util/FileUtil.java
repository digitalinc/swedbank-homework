package net.baghirzade.classfinder.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileUtil {

    public static List<String> readLines(String filePath) {

        List<String> lines = new ArrayList<>();

        try (FileInputStream inputStream = new FileInputStream(filePath);
             Scanner sc = new Scanner(inputStream, StandardCharsets.UTF_8)) {

            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }
}
