package net.baghirzade.classfinder;

import net.baghirzade.classfinder.exception.UndefinedPatternException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DefaultClassFinderTest {

    ClassFinder classFinder;

    String classListPath = "src/test/resources/class-list";

    @BeforeEach
    void setup() {
        classFinder = new DefaultClassFinder();
    }

    @Test
    void testCase_Basic() {

        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("a.b.FooBar");
        expectedResult.add("c.d.FooBarBuz");
        expectedResult.add("a.d.FooBarIsJustFooBar");

        assertEquals(expectedResult, classFinder.find(classListPath, "FooBar"));
    }

    @Test
    void testCase_EmptyPattern() {

        assertThrows(UndefinedPatternException.class, () -> {classFinder.find(classListPath, "");});
    }

    @Test
    void testCase_WithSpace() {
        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("a.b.FooBar");
        expectedResult.add("a.d.FooBarIsJustFooBar");

        assertEquals(expectedResult, classFinder.find(classListPath, "FooBar "));
    }

    @Test
    void testCase_WithWildcard() {
        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("a.m.HelloWorld");

        assertEquals(expectedResult, classFinder.find(classListPath, "H*oW"));
    }

    @Test
    void testCase_WithMultiWildcard() {
        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("a.m.HelloWorld");

        assertEquals(expectedResult, classFinder.find(classListPath, "H*oWo*d"));
    }

    @Test
    void testCase_WithWildcardAndSpace() {
        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("net.main.MainClassLoader");

        assertEquals(expectedResult, classFinder.find(classListPath, "MainL*r "));
    }

    @Test
    void testCase_WithPatternStartsWithLowercase() {
        assertThrows(UndefinedPatternException.class, () -> {classFinder.find(classListPath, "FooBar  ");});
    }
}
